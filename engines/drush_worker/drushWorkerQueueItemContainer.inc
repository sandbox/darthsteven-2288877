<?php

// Include the interface for this class.
drush_include_engine('drush_worker', 'drushWorkerQueueItemContainerInterface');

/**
 * This is a little class for passing work between Drush threads.
 *
 * It can make sure that the work is serializable into a JSON packet.
 */
class drushWorkerQueueItemContainer implements drushWorkerQueueItemContainerInterface {

  protected $data;

  /**
   * Take the data, and pop it somewhere safe.
   * @param $data
   */
  function __construct($data) {
    $this->data = $data;
  }

  /**
   * Return the data that this queue item contains.
   *
   * @return mixed
   */
  public function getData() {
    return $this->data;
  }


}
