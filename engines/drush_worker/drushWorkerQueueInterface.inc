<?php

interface DrushWorkerQueueInterface {

  /**
   * Retrieve an item in the queue for processing.
   *
   * @return
   *   On success we return an item object. If the queue is unable to claim an
   *   item it returns FALSE, we assume that there is no more work to claim.
   */
  public function fetchWork();
}
