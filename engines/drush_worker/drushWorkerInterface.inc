<?php

interface drushWorkerInterface {
  public function process(drushWorkerQueueItemContainerInterface $queue_item);
}
