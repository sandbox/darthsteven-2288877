<?php

/**
 * Asks the parent process for some work to do.
 */
function drush_worker_child_ask_for_item() {
  drush_worker_drush_backend_packet('drush_worker_master_get_item');
}

/**
 * Output a backend packet.
 *
 * @param packet
 *   The packet to send.
 * @param data
 *   Data for the command.
 */
function drush_worker_drush_backend_packet($packet, $data = array()) {
  $data['packet'] = $packet;
  $data = json_encode($data);
  // We use 'fwrite' instead of 'drush_print' here because
  // this backend packet is out-of-band data.
  fwrite(STDERR, sprintf(DRUSH_BACKEND_PACKET_PATTERN, $data));
}

/**
 * Parse out and remove backend packet from the supplied string and
 * invoke the commands.
 */
function drush_worker_child_drush_parse_packets(&$string, &$remainder, $backend_options) {
  $remainder = '';
  $packet_regex = strtr(sprintf(DRUSH_BACKEND_PACKET_PATTERN, "([^\0]*)"), array("\0" => "\\0"));
  $packet_regex = str_replace("\n", "", $packet_regex);
  if (preg_match_all("/$packet_regex/s", $string, $match, PREG_PATTERN_ORDER)) {
    drush_set_context('DRUSH_RECEIVED_BACKEND_PACKETS', TRUE);
    foreach ($match[1] as $packet_data) {
      $entry = (array) json_decode($packet_data);
      if (is_array($entry) && isset($entry['packet'])) {
        $function = 'drush_backend_packet_' . $entry['packet'];
        if (function_exists($function)) {
          $function($entry, $backend_options);
        }
        else {
          drush_log(dt("Unknown backend packet @packet", array('@packet' => $entry['packet'])), 'notice');
        }
      }
      else {
        drush_log(dt("Malformed backend packet"), 'error');
        drush_log(dt("Bad packet: @packet", array('@packet' => print_r($entry, TRUE))), 'debug');
        drush_log(dt("String is: @str", array('@str' => $packet_data), 'debug'));
      }
    }
    $string = preg_replace("/$packet_regex/s", '', $string);
  }
  // Check to see if there is potentially a partial packet remaining.
  // We only care about the last null; if there are any nulls prior
  // to the last one, they would have been removed above if they were
  // valid drush packets.
  $embedded_null = strrpos($string, "\0");
  if ($embedded_null !== FALSE) {
    // We will consider everything after $embedded_null to be part of
    // the $remainder string if:
    //   - the embedded null is less than strlen(DRUSH_BACKEND_OUTPUT_START)
    //     from the end of $string (that is, there might be a truncated
    //     backend packet header, or the truncated backend output start
    //     after the null)
    //   OR
    //   - the embedded null is followed by DRUSH_BACKEND_PACKET_START
    //     (that is, the terminating null for that packet has not been
    //     read into our buffer yet)
    if (($embedded_null + strlen(DRUSH_BACKEND_OUTPUT_START) >= strlen($string)) || (substr($string, $embedded_null + 1, strlen(DRUSH_BACKEND_PACKET_START)) == DRUSH_BACKEND_PACKET_START)) {
      $remainder = substr($string, $embedded_null);
      $string = substr($string, 0, $embedded_null);
    }
  }
}

/**
 * Handle a packet asking us to exit.
 *
 * We don't exit here, but we set a global flag that will get picked up by
 * the main loop, which will then exit gracefully.
 */
function drush_backend_packet_drush_worker_child_exit() {
  drush_log('Child asked to exit, exiting...', 'ok');
  // Set the context to exit, and we'll end the main loop.
  drush_set_context('drush_worker_process_active', FALSE);
}

/**
 * Handle a backend packet containing a new work item.
 *
 * @param $data
 */
function drush_backend_packet_drush_worker_child_item($data) {
  $item = unserialize($data['drush_worker_item_serialized']);
  // We got a worker item, send it onto the worker for processing.
  $worker = drush_get_context('drush_worker_worker');
  $worker->process($item);
  // Ask for some more work.
  drush_worker_child_ask_for_item();
}
