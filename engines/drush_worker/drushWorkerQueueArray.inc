<?php

drush_include_engine('drush_worker', 'drushWorkerQueueInterface');
drush_include_engine('drush_worker', 'drushWorkerQueueItemContainer');

/**
 * Very simple queue for processing a known array of items.
 *
 * Pass the array of items to the constructor, and then this queue will
 * emit instances of drushWorkerQueueItemContainer containing each item
 * as needed.
 *
 * Class drushWorkerQueueArray
 */
class drushWorkerQueueArray implements drushWorkerQueueInterface {

  protected $items = array();

  function __construct(array $items) {
    $this->items = $items;
  }

  /**
   * Retrieve an item in the queue for processing.
   *
   * @return
   *   On success we return an item object. If the queue is unable to claim an
   *   item it returns FALSE, we assume that there is no more work to claim.
   */
  public function fetchWork() {
    if (!empty($this->items)) {
      $item = array_shift($this->items);
      return new drushWorkerQueueItemContainer($item);
    }
    else {
      return FALSE;
    }
  }
}
