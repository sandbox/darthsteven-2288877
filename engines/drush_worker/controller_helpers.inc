<?php

/**
 * @file
 * This is a file of helpers for Drush worker controllers.
 * We have nice things like being able to spin up new processes, and send them
 * data.
 * We can also parse packets, and send them. Aces.
 */

/**
 * Invoke a command in a new process, targeting the site specified by
 * the provided site alias record.
 *
 * Use this function instead of drush_backend_invoke_sitealias,
 * drush_backend_invoke_args, or drush_backend_invoke_command
 * (all obsolete in drush 5).
 *
 * @param array $site_alias_record
 *  The site record to execute the command on.  Use '@self' to run on the current site.
 * @param string $command_name
 *  The command to invoke.
 * @param array $commandline_args
 *  The arguments to pass to the command.
 * @param array $commandline_options
 *  The options (e.g. --select) to provide to the command.
 * @param mixed $backend_options
 *   TRUE - integrate errors
 *   FALSE - do not integrate errors
 *   array - @see drush_backend_invoke_concurrent
 *     There are also several options that _only_ work when set in
 *     this parameter.  They include:
 *      'invoke-multiple'
 *        If $site_alias_record represents a single site, then 'invoke-multiple'
 *        will cause the _same_ command with the _same_ arguments and options
 *        to be invoked concurrently (e.g. for running concurrent batch processes).
 *      'concurrency'
 *        Limits the number of concurrent processes that will run at the same time.
 *        Defaults to '4'.
 *      'override-simulated'
 *        Forces the command to run, even in 'simulated' mode. Useful for
 *        commands that do not change any state on the machine, e.g. to fetch
 *        database information for sql-sync via sql-conf.
 *      'interactive'
 *        Overrides the backend invoke process to run commands interactively.
 *      'fork'
 *        Overrides the backend invoke process to run non blocking commands in
 *        the background. Forks a new process by adding a '&' at the end of the
 *        command. The calling process does not receive any output from the child
 *        process. The fork option is used to spawn a process that outlives its
 *        parent.
 *
 * @return
 *   If the command could not be completed successfully, FALSE.
 *   If the command was completed, this will return an associative
 *   array containing the results of the API call.
 *   @see drush_backend_get_result()
 *
 * Do not change the signature of this function!  drush_invoke_process
 * is one of the key Drush APIs.  See http://drupal.org/node/1152908
 */
function drush_worker_drush_invoke_process($site_alias_record, $command_name, $commandline_args = array(), $commandline_options = array(), $backend_options = TRUE) {
  if (is_array($site_alias_record) && array_key_exists('site-list', $site_alias_record)) {
    list($site_alias_records, $not_found) = drush_sitealias_resolve_sitespecs($site_alias_record['site-list']);
    if (!empty($not_found)) {
      drush_log(dt("Not found: @list", array("@list" => implode(', ', $not_found))), 'warning');
      return FALSE;
    }
    $site_alias_records = drush_sitealias_simplify_names($site_alias_records);
    foreach ($site_alias_records as $alias_name => $alias_record) {
      $invocations[] = array(
        'site' => $alias_record,
        'command' => $command_name,
        'args' => $commandline_args,
      );
    }
  }
  else {
    $invocations[] = array(
      'site' => $site_alias_record,
      'command' => $command_name,
      'args' => $commandline_args);
    $invoke_multiple = drush_get_option_override($backend_options, 'invoke-multiple', 0);
    if ($invoke_multiple) {
      $invocations = array_fill(0, $invoke_multiple, $invocations[0]);
    }
  }
  return drush_worker_drush_backend_invoke_concurrent($invocations, $commandline_options, $backend_options);
}

/**
 * Execute a new local or remote command in a new process.
 *
 * n.b. Prefer drush_invoke_process() to this function.
 *
 * @param invocations
 *   An array of command records to exacute. Each record should contain:
 *     'site':
 *       An array containing information used to generate the command.
 *         'remote-host'
 *            Optional. A remote host to execute the drush command on.
 *         'remote-user'
 *            Optional. Defaults to the current user. If you specify this, you can choose which module to send.
 *         'ssh-options'
 *            Optional.  Defaults to "-o PasswordAuthentication=no"
 *         'path-aliases'
 *            Optional; contains paths to folders and executables useful to the command.
 *         '%drush-script'
 *            Optional. Defaults to the current drush.php file on the local machine, and
 *            to simply 'drush' (the drush script in the current PATH) on remote servers.
 *            You may also specify a different drush.php script explicitly.  You will need
 *            to set this when calling drush on a remote server if 'drush' is not in the
 *            PATH on that machine.
 *     'command':
 *       A defined drush command such as 'cron', 'status' or any of the available ones such as 'drush pm'.
 *     'args':
 *       An array of arguments for the command.
 *     'options'
 *       Optional. An array containing options to pass to the remote script.
 *       Array items with a numeric key are treated as optional arguments to the
 *       command.
 *     'backend-options':
 *       Optional. Additional parameters that control the operation of the invoke.
 *         'method'
 *            Optional. Defaults to 'GET'.
 *            If this parameter is set to 'POST', the $data array will be passed
 *            to the script being called as a JSON encoded string over the STDIN
 *            pipe of that process. This is preferable if you have to pass
 *            sensitive data such as passwords and the like.
 *            For any other value, the $data array will be collapsed down into a
 *            set of command line options to the script.
 *         'integrate'
 *            Optional. Defaults to TRUE.
 *            If TRUE, any error statuses will be integrated into the current
 *            process. This might not be what you want, if you are writing a
 *            command that operates on multiple sites.
 *         'log'
 *            Optional. Defaults to TRUE.
 *            If TRUE, any log messages will be integrated into the current
 *            process.
 *         'output'
 *            Optional. Defaults to TRUE.
 *            If TRUE, output from the command will be synchronously printed to
 *            stdout.
 *         'drush-script'
 *            Optional. Defaults to the current drush.php file on the local
 *            machine, and to simply 'drush' (the drush script in the current
 *            PATH) on remote servers.  You may also specify a different drush.php
 *            script explicitly.  You will need to set this when calling drush on
 *            a remote server if 'drush' is not in the PATH on that machine.
 *          'dispatch-using-alias'
 *            Optional. Defaults to FALSE.
 *            If specified as a non-empty value the drush command will be
 *            dispatched using the alias name on the command line, instead of
 *            the options from the alias being added to the command line
 *            automatically.
 * @param common_options
 *    Optional. Merged in with the options for each invocation.
 * @param backend_options
 *    Optional. Merged in with the backend options for each invocation.
 * @param default_command
 *    Optional. Used as the 'command' for any invocation that does not
 *    define a command explicitly.
 * @param default_site
 *    Optional. Used as the 'site' for any invocation that does not
 *    define a site explicitly.
 * @param context
 *    Optional. Passed in to proc_open if provided.
 *
 * @return
 *   If the command could not be completed successfully, FALSE.
 *   If the command was completed, this will return an associative array containing the data from drush_backend_output().
 */
function drush_worker_drush_backend_invoke_concurrent($invocations, $common_options = array(), $common_backend_options = array(), $default_command = NULL, $default_site = NULL, $context = NULL) {
  $index = 0;

  // Slice and dice our options in preparation to build a command string
  $invocation_options = array();
  foreach ($invocations as $invocation)  {
    $site_record = isset($invocation['site']) ? $invocation['site'] : $default_site;
    // NULL is a synonym to '@self', although the latter is preferred.
    if (!isset($site_record)) {
      $site_record = '@self';
    }
    // If the first parameter is not a site alias record,
    // then presume it is an alias name, and try to look up
    // the alias record.
    if (!is_array($site_record)) {
      $site_record = drush_sitealias_get_record($site_record);
    }
    $command = isset($invocation['command']) ? $invocation['command'] : $default_command;
    $args = isset($invocation['args']) ? $invocation['args'] : array();
    $command_options = isset($invocation['options']) ? $invocation['options'] : array();
    $backend_options = isset($invocation['backend-options']) ? $invocation['backend-options'] : array();
    // If $backend_options is passed in as a bool, interpret that as the value for 'integrate'
    if (!is_array($common_backend_options)) {
      $integrate = (bool)$common_backend_options;
      $common_backend_options = array('integrate' => $integrate);
    }

    $command_options += $common_options;
    $backend_options += $common_backend_options;

    $backend_options = _drush_backend_adjust_options($site_record, $command, $command_options, $backend_options);

    // Insure that contexts such as DRUSH_SIMULATE and NO_COLOR are included.
    $command_options += _drush_backend_get_global_contexts($site_record);

    // Add in command-specific options as well
    $command_options += drush_worker_drush_command_get_command_specific_options($site_record, $command);

    // If the caller has requested it, don't pull the options from the alias
    // into the command line, but use the alias name for dispatching.
    if (!empty($backend_options['dispatch-using-alias']) && isset($site_record['#name'])) {
      list($post_options, $commandline_options, $drush_global_options) = _drush_backend_classify_options(array(), $command_options, $backend_options);
      $site_record_to_dispatch = '@' . ltrim($site_record['#name'], '@');
    }
    else {
      list($post_options, $commandline_options, $drush_global_options) = _drush_backend_classify_options($site_record, $command_options, $backend_options);
      $site_record_to_dispatch = '';
    }
    $site_record += array('path-aliases' => array());
    $site_record['path-aliases'] += array(
      '%drush-script' => NULL,
    );

    $site = (array_key_exists('#name', $site_record) && !array_key_exists($site_record['#name'], $invocation_options)) ? $site_record['#name'] : $index++;
    $invocation_options[$site] = array(
      'site-record' => $site_record,
      'site-record-to-dispatch' => $site_record_to_dispatch,
      'command' => $command,
      'args' => $args,
      'post-options' => $post_options,
      'drush-global-options' => $drush_global_options,
      'commandline-options' => $commandline_options,
      'command-options' => $command_options,
      'backend-options' => $backend_options,
    );
  }

  // Calculate the length of the longest output label
  $max_name_length = 0;
  $label_separator = '';
  if (!array_key_exists('no-label', $common_options) && (count($invocation_options) > 1)) {
    $label_separator = array_key_exists('label-separator', $common_options) ? $common_options['label-separator'] : ' >> ';
    foreach ($invocation_options as $site => $item) {
      $backend_options = $item['backend-options'];
      if (!array_key_exists('#output-label', $backend_options)) {
        if (is_numeric($site)) {
          $backend_options['#output-label'] = ' * [@self.' . $site;
          $label_separator = '] ';
        }
        else {
          $backend_options['#output-label'] = $site;
        }
        $invocation_options[$site]['backend-options']['#output-label'] = $backend_options['#output-label'];
      }
      $name_len = strlen($backend_options['#output-label']);
      if ($name_len > $max_name_length) {
        $max_name_length = $name_len;
      }
      if (array_key_exists('#label-separator', $backend_options)) {
        $label_separator = $backend_options['#label-separator'];
      }
    }
  }
  // Now pad out the output labels and add the label separator.
  $reserve_margin = $max_name_length + strlen($label_separator);
  foreach ($invocation_options as $site => $item) {
    $backend_options = $item['backend-options'] + array('#output-label' => '');
    $invocation_options[$site]['backend-options']['#output-label'] = str_pad($backend_options['#output-label'], $max_name_length, " ") . $label_separator;
    if ($reserve_margin) {
      $invocation_options[$site]['drush-global-options']['reserve-margin'] = $reserve_margin;
    }
  }

  // Now take our prepared options and generate the command strings
  $cmds = array();
  foreach ($invocation_options as $site => $item) {
    $site_record = $item['site-record'];
    $site_record_to_dispatch = $item['site-record-to-dispatch'];
    $command = $item['command'];
    $args = $item['args'];
    $post_options = $item['post-options'];
    $commandline_options = $item['commandline-options'];
    $command_options = $item['command-options'];
    $drush_global_options = $item['drush-global-options'];
    $backend_options = $item['backend-options'];
    $os = drush_os($site_record);
    // If the caller did not pass in a specific path to drush, then we will
    // use a default value.  For commands that are being executed on the same
    // machine, we will use DRUSH_COMMAND, which is the path to the drush.php
    // that is running right now.  For remote commands, we will run a wrapper
    // script instead of drush.php -- drush.bat on Windows, or drush on Linux.
    $drush_path = $site_record['path-aliases']['%drush-script'];
    $php = array_key_exists('php', $site_record) ? $site_record['php'] : (array_key_exists('php', $command_options) ? $command_options['php'] : NULL);
    $drush_command_path = drush_build_drush_command($drush_path, $php, $os, array_key_exists('remote-host', $site_record));
    $cmd = _drush_backend_generate_command($site_record, $drush_command_path . " " . _drush_backend_argument_string($drush_global_options, $os) . " " . $site_record_to_dispatch . " " . $command, $args, $commandline_options, $backend_options) . ' 2>&1';
    $cmds[$site] = array(
      'cmd' => $cmd,
      'post-options' => $post_options,
      'backend-options' => $backend_options,
    );
  }

  return _drush_worker_drush_backend_invoke($cmds, $common_backend_options, $context);
}

/**
 * Create a new pipe with proc_open, and attempt to parse the output.
 *
 * We use proc_open instead of exec or others because proc_open is best
 * for doing bi-directional pipes, and we need to pass data over STDIN
 * to the remote script.
 *
 * Exec also seems to exhibit some strangeness in keeping the returned
 * data intact, in that it modifies the newline characters.
 *
 * @param cmd
 *   The complete command line call to use.
 * @param post_options
 *   An associative array to json-encode and pass to the remote script on stdin.
 * @param backend_options
 *   Options for the invocation.
 *
 * @return
 *   If the command could not be completed successfully, FALSE.
 *   If one command was executed, this will return an associative array containing
 *   the data from drush_backend_output().
 *   If multiple commands were executed, this will return an associative array
 *   containing one item, 'concurrent', which will contain a list of the different
 *   backend invoke results from each concurrent command.
 */
function _drush_worker_drush_backend_invoke($cmds, $common_backend_options = array(), $context = NULL) {
  if (drush_get_context('DRUSH_SIMULATE') && !array_key_exists('override-simulated', $common_backend_options)) {
    foreach ($cmds as $cmd) {
      drush_print(dt('Simulating backend invoke: !cmd', array('!cmd' => $cmd['cmd'])));
    }
    return FALSE;
  }
  foreach ($cmds as $cmd) {
    drush_log(dt('Backend invoke: !cmd', array('!cmd' => $cmd['cmd'])), 'command');
  }
  if (array_key_exists('interactive', $common_backend_options) || array_key_exists('fork', $common_backend_options)) {
    foreach ($cmds as $cmd) {
      $exec_cmd = $cmd['cmd'];
      if (array_key_exists('fork', $common_backend_options)) {
        $exec_cmd .= ' --quiet &';
      }
      $ret = drush_shell_proc_open($exec_cmd);
    }
    return $ret;
  }
  else {
    $process_limit = drush_get_option_override($common_backend_options, 'concurrency', 1);
    $procs = _drush_worker_drush_backend_proc_open($cmds, $process_limit, $context);
    $procs = is_array($procs) ? $procs : array($procs);

    $ret = array();
    foreach ($procs as $site => $proc) {
      if (($proc['code'] == DRUSH_APPLICATION_ERROR) && isset($common_backend_options['integrate'])) {
        drush_set_error('DRUSH_APPLICATION_ERROR', dt("The external command could not be executed due to an application error."));
      }

      if ($proc['output']) {
        $values = drush_backend_parse_output($proc['output'], $proc['backend-options'], $proc['outputted']);
        $values['site'] = $site;
        if (is_array($values)) {
          if (empty($ret)) {
            $ret = $values;
          }
          elseif (!array_key_exists('concurrent', $ret)) {
            $ret = array('concurrent' => array($ret, $values));
          }
          else {
            $ret['concurrent'][] = $values;
          }
        }
        else {
          $ret = drush_set_error('DRUSH_FRAMEWORK_ERROR', dt("The command could not be executed successfully (returned: !return, code: !code)", array("!return" => $proc['output'], "!code" =>  $proc['code'])));
        }
      }
    }
  }
  return empty($ret) ? FALSE : $ret;
}

/**
 * Call an external command using proc_open.
 *
 * @param cmds
 *    An array of records containing the following elements:
 *      'cmd' - The command to execute, already properly escaped
 *      'post-options' - An associative array that will be JSON encoded
 *        and passed to the script being called. Objects are not allowed,
 *        as they do not json_decode gracefully.
 *      'backend-options' - Options that control the operation of the backend invoke
 *     - OR -
 *    An array of commands to execute. These commands already need to be properly escaped.
 *    In this case, post-options will default to empty, and a default output label will
 *    be generated.
 * @param data
 *    An associative array that will be JSON encoded and passed to the script being called.
 *    Objects are not allowed, as they do not json_decode gracefully.
 *
 * @return
 *   False if the command could not be executed, or did not return any output.
 *   If it executed successfully, it returns an associative array containing the command
 *   called, the output of the command, and the error code of the command.
 */
function _drush_worker_drush_backend_proc_open($cmds, $process_limit, $context = NULL) {
  $descriptorspec = array(
    0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
    1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
  );

  $open_processes = array();
  $bucket = array();
  $process_limit = max($process_limit, 1);
  $is_windows = drush_is_windows();
  // Loop through processes until they all close, having a nap as needed.
  $nap_time = 0;
  while (count($open_processes) || count($cmds)) {
    $nap_time++;
    if (count($cmds) && (count($open_processes) < $process_limit)) {
      // Pop the site and command (key / value) from the cmds array
      end($cmds);
      list($site, $cmd) = each($cmds);
      unset($cmds[$site]);

      if (is_array($cmd)) {
        $c = $cmd['cmd'];
        $post_options = $cmd['post-options'];
        $backend_options = $cmd['backend-options'];
      }
      else {
        $c = $cmd;
        $post_options = array();
        $backend_options = array();
      }
      $backend_options += array(
        '#output-label' => '',
        '#process-read-size' => 4096,
      );
      $process = array();
      drush_log($backend_options['#output-label'] . $c);
      $process['process'] = proc_open($c, $descriptorspec, $process['pipes'], null, null, array('context' => $context));
      if (is_resource($process['process'])) {
        if ($post_options) {
          fwrite($process['pipes'][0], json_encode($post_options)); // pass the data array in a JSON encoded string
        }
        // If we do not close stdin here, then we cause a deadlock;
        // see: http://drupal.org/node/766080#comment-4309936
        // If we reimplement interactive commands to also use
        // _drush_proc_open, then clearly we would need to keep
        // this open longer.
        //fclose($process['pipes'][0]);

        $process['info'] = stream_get_meta_data($process['pipes'][1]);
        stream_set_blocking($process['pipes'][1], FALSE);
        stream_set_timeout($process['pipes'][1], 1);
        $bucket[$site]['cmd'] = $c;
        $bucket[$site]['output'] = '';
        $bucket[$site]['remainder'] = '';
        $bucket[$site]['backend-options'] = $backend_options;
        $bucket[$site]['end_of_output'] = FALSE;
        $bucket[$site]['outputted'] = FALSE;
        $open_processes[$site] = $process;
      }
      // Reset the $nap_time variable as there might be output to process next
      // time around:
      $nap_time = 0;
    }
    // Set up to call stream_select(). See:
    // http://php.net/manual/en/function.stream-select.php
    // We can't use stream_select on Windows, because it doesn't work for
    // streams returned by proc_open.
    if (!$is_windows) {
      $ss_result = 0;
      $read_streams = array();
      $write_streams = array();
      $except_streams = array();
      foreach ($open_processes as $site => &$current_process) {
        if (isset($current_process['pipes'][1])) {
          $read_streams[] = $current_process['pipes'][1];
        }
      }
      // Wait up to 2s for data to become ready on one of the read streams.
      if (count($read_streams)) {
        $ss_result = stream_select($read_streams, $write_streams, $except_streams, 2);
        // If stream_select returns a error, then fallback to using $nap_time.
        if ($ss_result !== FALSE) {
          $nap_time = 0;
        }
      }
    }

    foreach ($open_processes as $site => &$current_process) {
      if (isset($current_process['pipes'][1])) {
        // Collect output from stdout
        $bucket[$site][1] = '';
        $info = stream_get_meta_data($current_process['pipes'][1]);

        if (!feof($current_process['pipes'][1]) && !$info['timed_out']) {
          $string = $bucket[$site]['remainder'] . fread($current_process['pipes'][1], $backend_options['#process-read-size']);
          $bucket[$site]['remainder'] = '';
          $output_end_pos = strpos($string, DRUSH_BACKEND_OUTPUT_START);
          if ($output_end_pos !== FALSE) {
            $trailing_string = substr($string, 0, $output_end_pos);
            $trailing_remainder = '';
            // If there is any data in the trailing string (characters prior
            // to the backend output start), then process any backend packets
            // embedded inside.
            if (strlen($trailing_string) > 0) {
              drush_worker_drush_backend_parse_packets($trailing_string, $trailing_remainder, $bucket[$site]['backend-options'], $current_process['pipes']);
            }
            // If there is any data remaining in the trailing string after
            // the backend packets are removed, then print it.
            if (strlen($trailing_string) > 0) {
              _drush_backend_print_output($trailing_string . $trailing_remainder, $bucket[$site]['backend-options']);
              $bucket[$site]['outputted'] = TRUE;
            }
            $bucket[$site]['end_of_output'] = TRUE;
          }
          if (!$bucket[$site]['end_of_output']) {
            drush_worker_drush_backend_parse_packets($string, $bucket[$site]['remainder'], $bucket[$site]['backend-options'], $current_process['pipes']);
            // Pass output through.
            _drush_backend_print_output($string, $bucket[$site]['backend-options']);
            if (strlen($string) > 0) {
              $bucket[$site]['outputted'] = TRUE;
            }
          }
          $bucket[$site][1] .= $string;
          $bucket[$site]['output'] .= $string;
          $info = stream_get_meta_data($current_process['pipes'][1]);
          flush();

          // Reset the $nap_time variable as there might be output to process
          // next time around:
          if (strlen($string) > 0) {
            $nap_time = 0;
          }
        }
        else {
          fclose($current_process['pipes'][1]);
          unset($current_process['pipes'][1]);
          // close the pipe , set a marker

          // Reset the $nap_time variable as there might be output to process
          // next time around:
          $nap_time = 0;
        }
      }
      else {
        // if both pipes are closed for the process, remove it from active loop and add a new process to open.
        $bucket[$site]['code'] = proc_close($current_process['process']);
        unset($open_processes[$site]);

        // Reset the $nap_time variable as there might be output to process next
        // time around:
        $nap_time = 0;
      }
    }

    // We should sleep for a bit if we need to, up to a maximum of 1/10 of a
    // second.
    if ($nap_time > 0) {
      usleep(max($nap_time * 500, 100000));
    }
  }
  return $bucket;
}

/**
 * Return all of the command-specific options defined in the given
 * options set for the specified command name.  Note that it is valid
 * to use the command name alias rather than the primary command name,
 * both in the parameter to this function, and in the options set.
 */
function drush_worker_drush_command_get_command_specific_options($options, $command_name, $prefix = '') {
  $result = array();
  $command_name = drush_worker_drush_command_normalize_name($command_name);
  if (isset($options[$prefix . 'command-specific'])) {
    foreach ($options[$prefix . 'command-specific'] as $options_for_command => $values) {
      if ($command_name == drush_command_normalize_name($options_for_command)) {
        $result = array_merge($result, $values);
      }
    }
  }
  return $result;
}

/**
 * @param string
 *   name of command or command alias.
 *
 * @return string
 *   Primary name of command.
 */
function drush_worker_drush_command_normalize_name($command_name) {
  $commands = drush_get_commands();
  return isset($commands[$command_name]) ? $commands[$command_name]['command'] : $command_name;
}

/**
 * Parse out and remove backend packet from the supplied string and
 * invoke the commands.
 */
function drush_worker_drush_backend_parse_packets(&$string, &$remainder, $backend_options, &$pipes) {
  $remainder = '';
  $packet_regex = strtr(sprintf(DRUSH_BACKEND_PACKET_PATTERN, "([^\0]*)"), array("\0" => "\\0"));
  $packet_regex = str_replace("\n", "", $packet_regex);
  if (preg_match_all("/$packet_regex/s", $string, $match, PREG_PATTERN_ORDER)) {
    drush_set_context('DRUSH_RECEIVED_BACKEND_PACKETS', TRUE);
    foreach ($match[1] as $packet_data) {
      $entry = (array) json_decode($packet_data);
      if (is_array($entry) && isset($entry['packet'])) {
        $function = 'drush_backend_packet_' . $entry['packet'];
        if (function_exists($function)) {
          $function($entry, $backend_options, $pipes);
        }
        else {
          drush_log(dt("Unknown backend packet @packet", array('@packet' => $entry['packet'])), 'notice');
        }
      }
      else {
        drush_log(dt("Malformed backend packet"), 'error');
        drush_log(dt("Bad packet: @packet", array('@packet' => print_r($entry, TRUE))), 'debug');
        drush_log(dt("String is: @str", array('@str' => $packet_data), 'debug'));
      }
    }
    $string = preg_replace("/$packet_regex/s", '', $string);
  }
  // Check to see if there is potentially a partial packet remaining.
  // We only care about the last null; if there are any nulls prior
  // to the last one, they would have been removed above if they were
  // valid drush packets.
  $embedded_null = strrpos($string, "\0");
  if ($embedded_null !== FALSE) {
    // We will consider everything after $embedded_null to be part of
    // the $remainder string if:
    //   - the embedded null is less than strlen(DRUSH_BACKEND_OUTPUT_START)
    //     from the end of $string (that is, there might be a truncated
    //     backend packet header, or the truncated backend output start
    //     after the null)
    //   OR
    //   - the embedded null is followed by DRUSH_BACKEND_PACKET_START
    //     (that is, the terminating null for that packet has not been
    //     read into our buffer yet)
    if (($embedded_null + strlen(DRUSH_BACKEND_OUTPUT_START) >= strlen($string)) || (substr($string, $embedded_null + 1, strlen(DRUSH_BACKEND_PACKET_START)) == DRUSH_BACKEND_PACKET_START)) {
      $remainder = substr($string, $embedded_null);
      $string = substr($string, 0, $embedded_null);
    }
  }
}

/**
 * Handle a packet from a child, asking for data.
 */
function drush_backend_packet_drush_worker_master_get_item($data, $backend_options, &$pipes) {
  static $queue;
  if (!isset($queue)) {
    // Lazily get a reference to our queue.
    $controller = drush_get_context('drush_worker_controller');
    $queue = $controller->getQueue();
  }

  // Fetch some more work from the queue.
  $work = $queue->fetchWork();
  if (!empty($work)) {
    drush_worker_drush_backend_input_packet($pipes[0], 'drush_worker_child_item', array('drush_worker_item_serialized' => serialize($work)));
  }
  else {
    // No more work, so ask this child thread to exit.
    drush_worker_drush_backend_input_packet($pipes[0], 'drush_worker_child_exit');
  }
}

/**
 * Output a backend packet if we're running as backend.
 *
 * @param packet
 *   The packet to send.
 * @param data
 *   Data for the command.
 */
function drush_worker_drush_backend_input_packet($pipe, $packet, $data = array()) {
  $data['packet'] = $packet;
  $data = json_encode($data);
  // We use 'fwrite' instead of 'drush_print' here because
  // this backend packet is out-of-band data.
  fwrite($pipe, sprintf(DRUSH_BACKEND_PACKET_PATTERN, $data));
}
