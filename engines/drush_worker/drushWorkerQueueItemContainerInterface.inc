<?php

interface drushWorkerQueueItemContainerInterface {

  /**
   * Return the data that this queue item contains.
   *
   * @return mixed
   */
  public function getData();

}
