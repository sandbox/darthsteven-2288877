<?php

interface drushWorkerMasterInterface {
  function __construct(drushWorkerQueueInterface $queue, $workerClassName, $workerArgs = array(), $workerEngine = '');

  public function run();
}
