<?php

drush_include_engine('drush_worker', 'drushWorkerMasterInterface');
drush_include_engine('drush_worker', 'drushWorkerQueueInterface');

class drushWorkerMaster implements drushWorkerMasterInterface {
  protected $queue;

  protected $childWorkerCommand = 'drush-worker-process';
  protected $childWorkerArgs;
  protected $childWorkerClassName;
  protected $childWorkerClassEngine;

  function __construct(drushWorkerQueueInterface $queue, $workerClassName, $workerArgs = array(), $workerEngine = '') {
    $this->queue = $queue;
    $this->childWorkerClassName = $workerClassName;
    $this->childWorkerArgs = $workerArgs;
    if (!empty($workerEngine)) {
      $this->childWorkerClassEngine = $workerEngine;
    }
    else {
      $this->childWorkerClassEngine = $workerClassName;
    }
  }

  /**
   * @return \drushWorkerQueueInterface
   */
  public function getQueue() {
    return $this->queue;
  }

  /**
   * @return string
   */
  public function getChildWorkerClassName() {
    return $this->childWorkerClassName;
  }

  /**
   * @return string
   */
  public function getChildWorkerCommand() {
    return $this->childWorkerCommand;
  }

  /**
   * @return array
   */
  public function getChildWorkerArgs() {
    return $this->childWorkerArgs;
  }

  /**
   * @return mixed
   */
  public function getChildWorkerClassEngine() {
    return $this->childWorkerClassEngine;
  }

  public function run() {
    // Load up the helper functions we'll need.
    drush_include_engine('drush_worker', 'controller_helpers');

    $concurrency = drush_get_option('concurrency', 1);
    $backend_options = array(
      'invoke-multiple' => $concurrency,
      'concurrency' => $concurrency,
    );

    $args = array();

    $options = array(
      'bootstrap' => drush_get_context('DRUSH_BOOTSTRAP_PHASE', DRUSH_BOOTSTRAP_DRUSH),
      'worker_class' => $this->getChildWorkerClassName(),
      // @TODO: Drush should be able to pass this as a native array.
      'worker_class_args' => serialize($this->getChildWorkerArgs()),
      'worker_class_engine' => $this->getChildWorkerClassEngine(),
    );

    // It pains me to do this, but the only sensible way to do this is to pop ourselves into
    // a lovely global, and then we can be easily accessed later, as needed.
    drush_set_context('drush_worker_controller', $this);

    drush_worker_drush_invoke_process('@self', $this->getChildWorkerCommand(), $args, $options, $backend_options);
  }
}
