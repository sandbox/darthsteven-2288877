<?php

/**
 * Implements hook_drush_engine_TYPE().
 *
 * We return some simple information about our provided engines.
 */
function drush_worker_search_api_drush_engine_drush_worker() {
  $engines = array(
    'drushWorkerSearchAPIQueue',
    'drushWorkerSearchAPIWorker',
  );

  $return = array();
  foreach ($engines as $engine) {
    $return[$engine] = array(
      'path' => dirname(__FILE__) . '/engines/drush_worker',
    );
  }

  return $return;
}

/**
 * Implements hook_drush_command().
 *
 * We provide some simple commands, mostly hidden, for internal use only.
 *
 * @return array
 */
function drush_worker_search_api_drush_command() {
  $items = array();

  $items['drush-worker-search-api-index'] = array(
    'drupal dependencies' => array(
      'search_api',
    ),
    'arguments' => array(
      'index' => array(
        'description' => 'The machine name of the Search API index to process.',
      ),
    ),
    // Use the options from the hidden 'master' command.
    'allow-additional-options' => array(
      'drush-worker-master-command-options',
    ),
  );

  return $items;
}

/**
 * Validation function for the search API index command.
 */
function drush_drush_worker_search_api_index_validate($index) {
  // Ensure we can load the SearchAPI index.
  $indexes = search_api_drush_get_index($index);
  if (!isset($indexes[$index])) {
    return drush_set_error('SEARCH_API_INDEX_NOT_FOUND', 'Unable to load specified SearchAPI index.');
  }
  else {
    // Pop it in a nice global so we don't need to load it again.
    drush_set_context('search_api_index', $indexes[$index]);
  }
}

function drush_drush_worker_search_api_index() {
  // Build up a queue of work.
  drush_include_engine('drush_worker', 'drushWorkerSearchAPIQueue');
  drush_include_engine('drush_worker', 'drushWorkerMaster');
  $index = drush_get_context('search_api_index');
  $queue = new drushWorkerSearchAPIQueue($index);
  $controller = new drushWorkerMaster($queue, 'drushWorkerSearchAPIWorker');
  $controller->run();
}

