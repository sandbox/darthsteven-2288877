<?php

// Include the interface for this class.
drush_include_engine('drush_worker', 'drushWorkerQueueInterface');

// Include the container for work.
drush_include_engine('drush_worker', 'drushWorkerQueueItemContainer');

class drushWorkerSearchAPIQueue implements drushWorkerQueueInterface {

  protected $index;
  protected $batchSize;

  function __construct(SearchApiIndex $index) {
    $this->index = $index;
    $this->batchSize = empty($this->index->options['cron_limit']) ? SEARCH_API_DEFAULT_CRON_LIMIT : $this->index->options['cron_limit'];
  }


  function fetchWork() {
    $ids = search_api_get_items_to_index($this->index, $this->batchSize);
    if (!empty($ids)) {
      $data = array(
        'index' => $this->index->identifier(),
        'items' => $ids,
      );
      return new DrushWorkerQueueItemContainer($data);
    }
    else {
      return FALSE;
    }
  }
}
