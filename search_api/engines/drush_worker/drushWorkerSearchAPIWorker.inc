<?php

drush_include_engine('drush_worker', 'drushWorkerInterface');

// Need to include the container for the data.
drush_include_engine('drush_worker', 'drushWorkerQueueItemContainer');

class drushWorkerSearchAPIWorker implements drushWorkerInterface {

  protected $indexes = array();

  public function process(drushWorkerQueueItemContainerInterface $queue_item) {
    $data = $queue_item->getData();
    $index_name = $data['index'];
    $items = $data['items'];
    $indexed_items = search_api_index_specific_items($this->getIndex($index_name), $items);
    drush_log(dt('Indexed @count items successfully', array('@count' => count($indexed_items))), 'ok');
  }

  /**
   * Return the Search API index object with the given machine name.
   *
   * @param $index_name
   *   The machine name of the index to load.
   *
   * @return mixed
   *   The Search API index object.
   * @throws Exception
   */
  protected function getIndex($index_name) {
    if (!isset($this->indexes[$index_name])) {
      $indexes = search_api_drush_get_index($index_name);
      if (!isset($indexes[$index_name])) {
        throw new Exception("Could not load specified Search API index");
      }
      $this->indexes[$index_name] = $indexes[$index_name];
    }

    return $this->indexes[$index_name];
  }

}