<?php

/**
 * Implements hook_drush_engine_type_info().
 */
function drush_worker_drush_engine_type_info() {
  return array(
    'drush_worker' => array(
      'description' => 'Base classes and functionality for Drush workers.',
      'default' => 'base',
    ),
  );
}

/**
 * Implements hook_drush_engine_TYPE().
 *
 * We return some simple information about our provided engines.
 */
function drush_worker_drush_engine_drush_worker() {
  $engines = array(
    // Our procedural code.
    'controller_helpers',
    'worker_helpers',

    // Our classes.
    'drushWorkerInterface',
    'drushWorkerMaster',
    'drushWorkerMasterInterface',
    'drushWorkerQueueArray',
    'drushWorkerQueueInterface',
    'drushWorkerQueueItemContainer',
    'drushWorkerQueueItemContainerInterface',
  );

  $return = array();
  foreach ($engines as $engine) {
    $return[$engine] = array(
      'path' => dirname(__FILE__) . '/engines/drush_worker',
    );
  }

  return $return;
}

/**
 * Implements hook_drush_command().
 *
 * We provide some simple commands, mostly hidden, for internal use only.
 *
 * @return array
 */
function drush_worker_drush_command() {
  $items = array();

  $items['drush-worker-process'] = array(
    'hidden' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'options' => array(
      'bootstrap' => 'The bootstrap level',
      'worker_class' => 'The worker class to load.',
      'worker_class_engine' => 'The engine name of that contains the worker class.',
      'worker_class_args' => 'Arguments to pass to the constructor of the worker class.'
    ),
  );

  // Add this command to your commands options to get the options available for
  // the master, like concurrency etc.
  $items['drush-worker-master-command-options'] = array(
    'options' => array(
      'concurrency' => 'The amount of worker processes to use to process the queue',
    ),
    'hidden' => TRUE,
  );

  return $items;
}

/**
 * Drush command for processing some work from a master command.
 */
function drush_drush_worker_process() {
  drush_include_engine('drush_worker', 'worker_helpers');
  // Bootstrap to the same level as our parent.
  $level = drush_get_option('bootstrap', DRUSH_BOOTSTRAP_DRUSH);
  drush_bootstrap($level);

  $worker_class = drush_get_option('worker_class');
  $worker_class_args = drush_get_option('worker_class_args');
  $worker_class_args = !empty($worker_class_args) ? unserialize($worker_class_args) : array();

  // Include the engine for the worker class.
  $worker_engine = drush_get_option('worker_class_engine');
  drush_include_engine('drush_worker', $worker_engine);

  // Spin up our worker class.
  $worker = new $worker_class($worker_class_args);

  // And stick it in a nasty global for later access.
  drush_set_context('drush_worker_worker', $worker);

  // Now wait for some work to do.
  drush_log('child process started', 'ok');

  drush_worker_child_ask_for_item();

  $string = '';
  $fp = fopen('php://stdin', 'r');

  stream_set_blocking($fp, FALSE);
  // Set up our arrays for stream_select.
  $read_streams = array($fp);
  $write_streams = array();
  $except_streams = array();

  // Packets can set this global flag to exit our loop.
  while (drush_get_context('drush_worker_process_active', TRUE)) {
    // Do a stream_select so that we block until there's something to read.
    stream_select($read_streams, $write_streams, $except_streams, 2);

    // Read the remaining bytes from the stream.
    $string .= stream_get_contents($fp);
    // Try to parse some packets.
    drush_worker_child_drush_parse_packets($string, $remainder, array());
    // Any $remainder, is valid data to be processed next time around.
    $string .= $remainder;
  }

  // Close our open handle.
  fclose($fp);

  // Gracefully exit.
  drush_log('Process exit', 'info');
}
