<?php

drush_include_engine('drush_worker', 'drushWorkerInterface');
drush_include_engine('drush_worker', 'drushWorkerQueueItemContainer');

class drushWorkerExampleWorker implements drushWorkerInterface {

  function process(drushWorkerQueueItemContainerInterface $queue_item) {
    // We're just going to sleep for the specified amount of time.
    $time = $queue_item->getData();
    drush_log(dt('Sleeping for @count seconds', array('@count' => $time / 10000000)), 'ok');
    usleep($time);
  }

}
