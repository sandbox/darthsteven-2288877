<?php

/**
 * Implements hook_drush_engine_TYPE().
 *
 * We return some simple information about our provided engines.
 */
function drush_worker_example_drush_engine_drush_worker() {
  $engines = array(
    'drushWorkerExampleWorker',
  );

  $return = array();
  foreach ($engines as $engine) {
    $return[$engine] = array(
      'path' => dirname(__FILE__) . '/engines/drush_worker',
    );
  }

  return $return;
}

/**
 * Implements hook_drush_command().
 *
 * We provide some simple commands, mostly hidden, for internal use only.
 *
 * @return array
 */
function drush_worker_example_drush_command() {
  $items = array();

  $items['drush-worker-example'] = array(
    // Use the options from the hidden 'master' command.
    'allow-additional-options' => array(
      'drush-worker-master-command-options',
    ),
  );

  return $items;
}

/**
 * Drush command to do some example work.
 */
function drush_drush_worker_example() {
  // Build up a queue of work.
  drush_include_engine('drush_worker', 'drushWorkerQueueArray');
  // Generate an array of times to sleep for.
  $times = range(500 * 100, 5000 * 100, 500);
  shuffle($times);
  $queue = new drushWorkerQueueArray($times);

  // Create our master controller.
  drush_include_engine('drush_worker', 'drushWorkerMaster');
  $controller = new drushWorkerMaster($queue, 'drushWorkerExampleWorker');
  $controller->run();
}

